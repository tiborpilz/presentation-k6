---
title: Load Testing mit K6
author: Tibor Pilz
date: 06.12.2021
---

## Was ist Load Testing?

- Testet das Verhalten eines Systems unter Last
- In den meisten Fällen: Simuliert mehrere User
- Performance (Antwortzeit, Ressourcenauslastung, etc.)
- Überprüft auch Fehler

---

## Warum Load Testing?

- Performance ist immer abhängig von der Anzahl der Nutzer
- Eine hohe Last verlangsamt ein System stärker als nur per Datendurchsatz
- Ein System skaliert nur im besten Fall linear
- Exponentielle Bottlenecks können erst bei hoher Nutzerzahl sichtbar sein
- Viele Bugs (Concurrency, DB Deadlock, etc.) treten erst bei hoher Last auf

---

## Was testen?

- Einzelne Strecken:
  - z.B. Login:
    1. Verbindung Login-Seite
    2. Login-Request
    3. Verbindung zum User-Dashboard
- Strecken als "Paket"
- API-Requests immer
- Asset-Requests manchmal
- Für jede Strecke Failconditions definieren

::: notes

Von der technischen Seite ist der Login vielleicht nur das Schicken der Credentials an den Endpunkt, aber
für den User sind es mehrere Schritte. Deswegen macht es auch Sinn, die Gesamtzeit zu messen, da es für den
User egal ist, ob nun der Login-Request oder der Aufbau der Login-Seite lange gedauert hat.

:::

---

## Was messen / wie auswerten?

- Fehlerquote (pro definiertem Fehler)
- Performancedaten:
  - Mittelwert der Verbindungszeit
  - Median der Verbindungszeit
  - Perzentile (z.B. P90, P95)
  - Min / Max

::: notes

Der Mittelwert ist keine aussagekräftige Größe, und wird eher der Vollständigkeit halber mit angegeben.
Der Median ist hier sinnvoller, da er nicht so stark von Ausreissern beeinflusst wird.
Das 90. Perzentil nimmt die schlechtesten 10 % raus, und gibt dann den schlechtesten Wert an. Perzentile sind sinnvoll,
um eine Minimalqualität für einen Großteil der Nutzer sicherzustellen, ohne komplett von Aussreissern beeinflusst zu sein.
Min/Max sind eher der Vollständigkeit halber mit dabei, und können zusätzliche Informationen liefern. (Ist z.B. ein Extremwert von > 1 Min dabei.)

:::

---

## Wie testen?

### Curl - Einfacher Aufruf

```bash
curl -s -o /dev/null -w $'%{time_total}\n' https://www.google.de
```

Output:
```
0.193175
```

::: notes

Curl wird per `-s` auf 'silent' gestellt, und der Output wird per `-o` nach `/dev/null` geleitet.
Per `-w $'%{time_total}\n'` wird ein zusätzlicher Output definiert, der die Gesamtverbindungszeit nach stdout schickt.
Die Ausgabe ist dann die Gesamtverbindungszeit in Sekunden.

:::

---

## Wie testen?

### Curl - GNU Parallel

```bash
URL=https://www.google.de
N=20

seq $N \
  | parallel -j $N "curl -o /dev/null -sL -w $'{}: %{time_total}\n' $URL" \
  | sort -n
  > out_$N.log
```

```
1:      0.263425
2:      0.262657
3:      0.258604
4:      0.249628
...
```

::: notes

GNU Parallel führt Befehle parallel aus. Es ist sehr vielseitig, und genau darauf einzugehen würde diesen Rahmen sprengen.
Per `seq $N` wird ein Array von der Größe `$N` definiert, welches dann in `parallel` gefüttert wird, um `$N` parallele Jobs zu definieren.
`parallel` ruft dann den oben beschriebenen `curl` Befehl auf und der Gesamtoutput wird nach Job-Index sortiert und in eine Logfile geschrieben.

:::

---

## Wie testen?

### Curl - GNU Parallel - GNU Datamash

```bash
cat out_20.log \
  | cut -d $'\t' -f 2 \
  | datamash mean 1 median 1 max 1 min 1 --header-out
```

```
mean(field-1)   median(field-1) max(field-1)    min(field-1)
0.22805215      0.2242635       0.268756        0.188365
```

::: notes
GNU Datamash ist ein Kommandozeilentool um einfache Datenverarbeitung zu machen, hier eignet es sich um den Output von `parallel` weiterzuverarbeiten.
:::

---

## Wie testen?

### Curl

- Für simple Fälle können Shellscripte ausreichend sein
- Komplexe Fälle sprengen sehr schnell den Rahmen:
  - Strecken mit mehr als einem Request
  - Authentifizierung
  - Cookies
  - Fehlerdefinition
  - HTML Parsing
  - Dynamische Daten
  - ...

::: notes
Komplexe Fälle sind in diesem Beispiel schon Strecken mit mehr als einem Request.
:::

---

## Tools

  - WebLOAD
  - Apache JMeter
  - Gatling
  - Locust

::: notes

WebLOAD:
  - Großes Enterprise-Tool, Self-Hosted oder SaaS
  - "AI-Features"
  - Kostenfaktor: "Preis anfragen..."

JMeter:
  - Open-Source
  - GUI und CLI
  - Unterstützt viele Technologien, z.B.: Java Object, Web HTTP/HTTPS/SOAP und REST, FTP
  - Syntax in Groovy

Gatling:
  - Open-Source / Enterprise / Enterprise SaaS
  - Tests werden in einer eigenen Domain Specific Language geschrieben

Locust:
  - Open-Source
  - Tests werden in Python geschrieben
  - Web UI

Configuration as Code ist immer ein  Pluspunkt - allerdings ist hier natürlich die Sprache wichtig.
Von den Tools bieten 3 eine Code-basierte Konfigurierbarkeit, davon ist Python für mich noch am interessantesten.

:::

---

## K6

- Key Features:
  - CLI
  - Scripting in Javascript
  - Checks und Thresholds
- Open Source / Enterprise SaaS
- Use cases:
  - Load Testing
  - Performance Monitoring

::: notes

K6 wurde 2021 von Grafan Labs übernommen.

:::

---

## K6

```{.javascript .data-line-numbers}
import k6 from 'k6';

export default function () {
  http.get('https://google.com');
}
```

---

## K6

```{.log .code-more data-line-numbers=[|30|]}
          /\      |‾‾| /‾‾/   /‾‾/
     /\  /  \     |  |/  /   /  /
    /  \/    \    |     (   /   ‾‾\
   /          \   |  |\  \ |  (‾)  |
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: google.js
     output: -

  scenarios: (100.00%) 1 scenario, 1 max VUs, 10m30s max duration (incl. graceful stop):
           * default: 1 iterations for each of 1 VUs (maxDuration: 10m0s, gracefulStop: 30s)


running (00m00.6s), 0/1 VUs, 1 complete and 0 interrupted iterations
default ✓ [======================================] 1 VUs  00m00.6s/10m0s  1/1 iters, 1 per VU

     data_received..................: 31 kB  50 kB/s
     data_sent......................: 1.9 kB 3.1 kB/s
     http_req_blocked...............: avg=153.12ms min=98.84ms  med=107.4ms  max=253.13ms p(90)=223.98ms p(95)=238.56ms
     http_req_connecting............: avg=30.2ms   min=29.28ms  med=29.96ms  max=31.35ms  p(90)=31.07ms  p(95)=31.21ms
     http_req_duration..............: avg=52.46ms  min=37.81ms  med=54.15ms  max=65.43ms  p(90)=63.18ms  p(95)=64.31ms
       { expected_response:true }...: avg=52.46ms  min=37.81ms  med=54.15ms  max=65.43ms  p(90)=63.18ms  p(95)=64.31ms
     http_req_failed................: 0.00%  ✓ 0        ✗ 3
     http_req_receiving.............: avg=1.73ms   min=118µs    med=650µs    max=4.42ms   p(90)=3.67ms   p(95)=4.05ms
     http_req_sending...............: avg=146.66µs min=117µs    med=161µs    max=162µs    p(90)=161.8µs  p(95)=161.9µs
     http_req_tls_handshaking.......: avg=87.37ms  min=37.88ms  med=39.16ms  max=185.08ms p(90)=155.9ms  p(95)=170.49ms
     http_req_waiting...............: avg=50.59ms  min=37ms     med=53.92ms  max=60.84ms  p(90)=59.46ms  p(95)=60.15ms
     http_reqs......................: 3      4.855508/s
     iteration_duration.............: avg=616.85ms min=616.85ms med=616.85ms max=616.85ms p(90)=616.85ms p(95)=616.85ms
     iterations.....................: 1      1.618503/s
```

::: notes

Hier ist vor allem die `iteration_duration` relevant, die Dauer eines Durchlaufs.

:::

---

## K6 - Beispiel FürSie

```{.javascript .code-more data-line-numbers=[|12-26|5|37|]}
import {group} from 'k6'
import encoding from 'k6/encoding'
import login from './groups/login.js'
import archiv from './groups/archiv.js'
import lieferanten from './groups/lieferanten.js'
import belege from './groups/belege.js'
import statistiken from './groups/statistiken.js'
import startseite from './groups/startseite.js'

import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js'

const VUS=__ENV.VUS || 1
const RUN=__ENV.RUN || 'ad-hoc'

export let options = {
  vus: VUS,
  duration: '5m',
  noVUConnectionReuse: true,
  tags: {
    name: `${VUS}`,
    run: `${RUN}`
  },
  hosts: {
    'example.com': '127.0.0.1',
  },
}

export default function (data) {
  let timestamp=Date.now()
  let windowName = ''
  let pageCounter = 0

  group('all tests', function() {
    group('login', () => [pageCounter,timestamp,windowName] = login(pageCounter, timestamp, windowName))
    group('archiv', () => [pageCounter,timestamp] = archiv(pageCounter, timestamp, windowName))
    group('startseite', () => [pageCounter, timestamp] = startseite(pageCounter, timestamp, windowName))
    group('lieferanten', () => [pageCounter, timestamp] = lieferanten(pageCounter, timestamp, windowName))
    group('belege', () => [pageCounter, timestamp] = belege(pageCounter, timestamp, windowName))
    group('statistiken', () => [pageCounter, timestamp] = statistiken(pageCounter, timestamp, windowName))
  })
}

export function handleSummary(data) {
  return {
    [`./logs/${VUS}.log`]: textSummary(data, { indent: '', enableColors: false }),
  }
}
```

::: notes

In dem Beispiel sieht man, wie die Optionen innerhalb des Scripts gesetzt werden können.
Um die Anzahl der Virtuellen Nutzer (VU) als Tag verwenden zu können, wird die Anzahl als Umgebungsvariable hineingegeben.

:::

---

## K6 - Beispiel FürSie - Lieferanten

```{.javascript .code-more data-line-numbers=[|10-17|11|14-16|19-52|31-34,41-42|44-48|]}
import http from 'k6/http'
import {check, group} from 'k6'

const headers = {
  'Wicket-Ajax': true,
  'Wicket-Ajax-BaseURL': 'lieferanteneinstieg',
}

export default function(pageCounter, timestamp, windowName) {
  group('visit', function() {
    let response = http.get('example.com')
    pageCounter += 1

    check(response, {
      'status is 200': r => r.status === 200,
    })
  })

  group('search', function() {
    timestamp = Date.now()
    let response = http.get(
      `https://example.com/lieferanteneinstieg?${pageCounter}-1.0-&windowName=${windowName}&_=${timestamp}`,
      {headers},
    )

    check(response, {
      'status is 200': r => r.status === 200,
      'body equals known response': r => r.body === '<?xml version="1.0" encoding="UTF-8"?><ajax-response></ajax-response>',
    })

    response = http.get(
      `https://example.com/lieferanteneinstieg?${pageCounter}-1.-form-lieferantenChoice&q=`,
      {headers},
    )
    check(response, {
      'status is 200': r => r.status === 200,
      'body starts with well known string': r => r.body.startsWith('{"items":[{"id":"'),
      'body ends with well known string': r => r.body.endsWith('}'),
    })

    const searchResults = JSON.parse(response.body).items
    const lieferant = searchResults[Math.floor(Math.random() * (searchResults.length - 1))]

    response = http.post(
      `https://example.com/lieferanteneinstieg?${pageCounter}-1.0-form-lieferantenChoice`,
      {lieferantenChoice: lieferant.id},
      {headers},
    )

    check(response, {
      'status is 200': r => r.status === 200,
    })
  })
```

::: notes

Die erste Untergruppe ist `visit`, die zweite `search`. In `visit` wird die Lieferantenseite besucht,
in `search` wird aus der Auswahl der möglichen Lieferanten per Zufall eine Lieferanten-ID ausgewählt und per Suchformular abgeschickt.

:::

---

## Auswertung in Grafana

![Grafana](images/grafana.jpg)

---

## Auswertung für den Kunden

![Auswertung](images/auswertung.png)

---

## Ergebnis

- Performanceprobleme wurden sichtbar
- Versteckte Fehler wurden entdeckt
- Sicherheitsprobleme wurden aufgedeckt

---

## Links

- https://k6.io/
- https://github.com/avenga/fuersielasttest
- https://github.com/grafana/k6-template-typescript
- https://tiborpilz.gitlab.io/presentation-k6
- https://gitlab.com/tiborpilz/presentation-k6/

# Fin

Fragen?
